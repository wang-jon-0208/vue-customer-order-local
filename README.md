# vue-customer-order
[history commit(gitlab)](https://gitlab.com/wang-jon-0208/vuecustomerordertest)

[history commit(github)](https://github.com/zero11995/VueCustomerOrderTest)

## Prerequisite

1.
[NodeJS](https://nodejs.org/en/download)

2.
```
npm install -g yarn
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
